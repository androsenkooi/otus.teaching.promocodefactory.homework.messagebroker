﻿using MassTransit;
using Otus.Teaching.Pcf.GivingToCustomer.Integration;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Services;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Consumers;

public class GivePromocodeToCustomerConsumer : IConsumer<GivePromoCodeToCustomerDto>
{
    private readonly IPromocodesService _service;

    public GivePromocodeToCustomerConsumer(IPromocodesService service)
    {
        _service = service;
    }

    public async Task Consume(ConsumeContext<GivePromoCodeToCustomerDto> context)
    {
        await _service.GivePromoCodesToCustomersWithPreferenceAsync(context.Message);
    }

}
