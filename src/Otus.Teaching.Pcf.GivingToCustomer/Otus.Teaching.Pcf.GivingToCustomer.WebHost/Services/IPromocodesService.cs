﻿using Otus.Teaching.Pcf.GivingToCustomer.Integration;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Services;

public interface IPromocodesService
{
    public Task GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeToCustomerDto dto);
}
