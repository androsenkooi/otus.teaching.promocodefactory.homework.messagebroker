﻿using System;

namespace Otus.Teaching.Pcf.Administration.WebHost.Models
{
    public class EmployeeDto
    {
        public Guid Id { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public Guid RoleId { get; set; }
        public string RoleName { get; set; }
        public string RoleDescription { get; set; }
        public int AppliedPromocodesCount { get; set; }
    }
}
