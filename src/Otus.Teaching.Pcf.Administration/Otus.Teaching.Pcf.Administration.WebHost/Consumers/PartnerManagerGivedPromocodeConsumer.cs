﻿using MassTransit;
using Otus.Teaching.Pcf.Administration.Integration;
using Otus.Teaching.Pcf.Administration.WebHost.Services;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.WebHost.Consumers
{
    public class PartnerManagerGivedPromocodeConsumer : IConsumer<GivePromoCodeToCustomerDto>
    {
        private readonly IEmployeeService _employeeService;

        public PartnerManagerGivedPromocodeConsumer(IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }

        public async Task Consume(ConsumeContext<GivePromoCodeToCustomerDto> context)
        {
            if (context.Message.PartnerManagerId.HasValue)
                await _employeeService.IncrementPromocodesCountAsync(context.Message.PartnerManagerId.Value);
        }
    }
}
