﻿using System.Threading.Tasks;
using System;

namespace Otus.Teaching.Pcf.Administration.WebHost.Services;

public interface IEmployeeService
{
    Task<bool> IncrementPromocodesCountAsync(Guid employeeId);
}
