﻿namespace SharedModels;

public class PartnerManagerGivedPromocodeDTO
{
    public Guid PartnerManagerId { get; set; }
}
